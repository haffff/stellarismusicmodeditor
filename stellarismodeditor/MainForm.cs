﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace stellarismodeditor
{
    public partial class MainForm : Form
    {

        private string selectedFile;
        //Name   Path
        public Dictionary<string, string> songs;
        public Dictionary<string, string> toInstallSongs;


        public MainForm()
        {
            InitializeComponent();
            songs = new Dictionary<string, string>();
            toInstallSongs = new Dictionary<string, string>();
            if (!Directory.Exists(Path.GetDirectoryName(Application.ExecutablePath) + "\\music\\"))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(Application.ExecutablePath) + "\\music\\");
            }
            string[] list = Directory.GetFiles(Path.GetDirectoryName(Application.ExecutablePath)+"\\music\\");
            foreach (string file in list)
            {
                songs.Add(file.Remove(file.LastIndexOf('.')).Substring(file.LastIndexOf('\\') + 1),file);
            }
            refreshLists();

        }

        public void refreshLists()
        {
            songList.Items.Clear();
            songList.Items.AddRange(songs.Keys.ToArray());
            toInstallSongsList.Items.Clear();
            toInstallSongsList.Items.AddRange(toInstallSongs.Keys.ToArray());
        }




 

        private void songList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(songList.SelectedItem==null)
            {
                return;
            }
            selectedFile = songs[songList.SelectedItem.ToString()];
        }


        private void cutButtonClick(object sender, EventArgs e)
        {
            if (selectedFile == null || songList.SelectedItem == null)
            {
                MessageBox.Show("Select file first");
                return;
            }
            Splitter.SplitterForm splitter = new Splitter.SplitterForm(selectedFile, this);
            splitter.Show();
        }

        private void addExternalMusicButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "All music files|*.mp3;*.wav;*.ogg|" +
                "Ogg Vorbis files|*.ogg|" +
                "Wavefront files |*.wav|" +
                "Mp3 Files|*.mp3|" +
                "All files|*.*";
            dialog.Multiselect = true;
            dialog.ShowDialog();
            foreach(string line in dialog.FileNames)
            {
                songs.Add(line.Remove(line.LastIndexOf('.')).Substring(line.LastIndexOf('\\')+1), line);
            }
            refreshLists();
        }

        private void songList_DoubleClick(object sender, EventArgs e)
        {
            if (songList.SelectedItem == null)
                return;
            toInstallSongsList.Items.Add(songList.SelectedItem);
            string songKey = songList.SelectedItem.ToString();

            toInstallSongs.Add(songKey, songs[songKey]);
            songs.Remove(songKey);

            songList.Items.Remove(songList.SelectedItem);
        }

        private void toInstallSongsList_DoubleClick(object sender, EventArgs e)
        {
           if (toInstallSongsList.SelectedItem == null)
                return;
            songList.Items.Add(toInstallSongsList.SelectedItem);

            string songKey = toInstallSongsList.SelectedItem.ToString();
            songs.Add(songKey, toInstallSongs[songKey]);
            toInstallSongs.Remove(songKey);

            toInstallSongsList.Items.Remove(toInstallSongsList.SelectedItem);
        }

        private void addAllButton_Click(object sender, EventArgs e)
        {
            if(songList==null) { return; }

            toInstallSongsList.Items.AddRange(songList.Items);

            foreach(KeyValuePair<string,string> a in songs)
            {
                toInstallSongs.Add(a.Key, a.Value);
            }

            songs.Clear();
            songList.Items.Clear();
        }

        private void clearbufor_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("This function will delete files from bufor.(f.e. splitted ones)\n Songs added externally will remains unchanged", "WARNING", MessageBoxButtons.YesNo);
            if(result == DialogResult.Yes)
            {
                string [] list = Directory.GetFiles(Path.GetDirectoryName(Application.ExecutablePath) + "\\music\\");
                foreach (string file in list)
                {
                    File.Delete(file);
                    string fileName = file.Remove(file.LastIndexOf('.')).Substring(file.LastIndexOf('\\') + 1);
                    songs.Remove(fileName);
                    songList.Items.Remove(fileName);
                }
            }
        }

        private void convertButton_Click(object sender, EventArgs e)
        {
            Converter.ToMod(toInstallSongs,nameBox.Text);
        }

        private void fastconvertCheckbox_Checked(object sender, EventArgs e)
        {
            Ffmpeg.nofastConvert = checkBox2.Checked;
        }
    }
}
