﻿namespace stellarismodeditor
{
    partial class MainForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.addExternalMusicButton = new System.Windows.Forms.Button();
            this.songList = new System.Windows.Forms.ListBox();
            this.toInstallSongsList = new System.Windows.Forms.ListBox();
            this.addAllButton = new System.Windows.Forms.Button();
            this.cutButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.replaceCheckBox = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // addExternalMusicButton
            // 
            this.addExternalMusicButton.Location = new System.Drawing.Point(12, 12);
            this.addExternalMusicButton.Name = "addExternalMusicButton";
            this.addExternalMusicButton.Size = new System.Drawing.Size(120, 23);
            this.addExternalMusicButton.TabIndex = 0;
            this.addExternalMusicButton.Text = "Add external music";
            this.addExternalMusicButton.UseVisualStyleBackColor = true;
            this.addExternalMusicButton.Click += new System.EventHandler(this.addExternalMusicButton_Click);
            // 
            // songList
            // 
            this.songList.FormattingEnabled = true;
            this.songList.Location = new System.Drawing.Point(12, 41);
            this.songList.Name = "songList";
            this.songList.Size = new System.Drawing.Size(241, 446);
            this.songList.TabIndex = 1;
            this.songList.SelectedIndexChanged += new System.EventHandler(this.songList_SelectedIndexChanged);
            this.songList.DoubleClick += new System.EventHandler(this.songList_DoubleClick);
            // 
            // toInstallSongsList
            // 
            this.toInstallSongsList.FormattingEnabled = true;
            this.toInstallSongsList.Location = new System.Drawing.Point(376, 41);
            this.toInstallSongsList.Name = "toInstallSongsList";
            this.toInstallSongsList.Size = new System.Drawing.Size(230, 446);
            this.toInstallSongsList.TabIndex = 2;
            this.toInstallSongsList.DoubleClick += new System.EventHandler(this.toInstallSongsList_DoubleClick);
            // 
            // addAllButton
            // 
            this.addAllButton.Location = new System.Drawing.Point(259, 126);
            this.addAllButton.Name = "addAllButton";
            this.addAllButton.Size = new System.Drawing.Size(111, 23);
            this.addAllButton.TabIndex = 3;
            this.addAllButton.Text = ">>";
            this.addAllButton.UseVisualStyleBackColor = true;
            this.addAllButton.Click += new System.EventHandler(this.addAllButton_Click);
            // 
            // cutButton
            // 
            this.cutButton.Location = new System.Drawing.Point(138, 12);
            this.cutButton.Name = "cutButton";
            this.cutButton.Size = new System.Drawing.Size(120, 23);
            this.cutButton.TabIndex = 4;
            this.cutButton.Text = "Cut/Split";
            this.cutButton.UseVisualStyleBackColor = true;
            this.cutButton.Click += new System.EventHandler(this.cutButtonClick);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(12, 493);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(115, 23);
            this.clearButton.TabIndex = 5;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearbufor_Click);
            // 
            // replaceCheckBox
            // 
            this.replaceCheckBox.AutoSize = true;
            this.replaceCheckBox.Location = new System.Drawing.Point(349, 16);
            this.replaceCheckBox.Name = "replaceCheckBox";
            this.replaceCheckBox.Size = new System.Drawing.Size(263, 17);
            this.replaceCheckBox.TabIndex = 6;
            this.replaceCheckBox.Text = "Replace music(mod will delete original soundtrack)";
            this.replaceCheckBox.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button3.Location = new System.Drawing.Point(482, 493);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(124, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = "Convert";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // nameBox
            // 
            this.nameBox.Location = new System.Drawing.Point(376, 496);
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(100, 20);
            this.nameBox.TabIndex = 8;
            this.nameBox.Text = "Mymod";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.checkBox2.Location = new System.Drawing.Point(12, 522);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(529, 17);
            this.checkBox2.TabIndex = 9;
            this.checkBox2.Text = "Use Ffmpeg with one file after another(Much slower, but doesn\'t make grill from y" +
    "our pc )";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.fastconvertCheckbox_Checked);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 537);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.nameBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.replaceCheckBox);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.cutButton);
            this.Controls.Add(this.addAllButton);
            this.Controls.Add(this.toInstallSongsList);
            this.Controls.Add(this.songList);
            this.Controls.Add(this.addExternalMusicButton);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Stellaris music editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addExternalMusicButton;
        private System.Windows.Forms.ListBox songList;
        private System.Windows.Forms.ListBox toInstallSongsList;
        private System.Windows.Forms.Button addAllButton;
        private System.Windows.Forms.Button cutButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.CheckBox replaceCheckBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.CheckBox checkBox2;
    }
}

