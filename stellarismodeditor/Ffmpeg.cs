﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stellarismodeditor
{
    public class Ffmpeg
    {
        static public bool nofastConvert = true;
        string prefix;
        string commandWithArgs;

        public Ffmpeg(string commandWithArgs, string prefix = "-i")
        {
            this.commandWithArgs = commandWithArgs;
            this.prefix = prefix;
        }

        //Replacers mean things that are replaced in command. for example "ffmpeg -i {INPUT} -ss [0] -to [1] -c copy {OUTPUT}"
        public string executeCommand(string fileIn, List<string> replacers, string fileOut = null )
        {

            if(!File.Exists("ffmpeg\\bin\\ffmpeg"))
            {
                MessageBox.Show("Can't launch FFMPEG. are you sure that you installed it?","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                return "";
            }

            string tmpCommand = commandWithArgs;
            if (replacers != null)
            foreach(string a in replacers)
            {
                tmpCommand = tmpCommand.Replace("[" + replacers.IndexOf(a) + "]", a);
            }
            if(fileOut == null)
            {
                fileOut = "..\\..\\music\\" + DateTime.Now.ToShortTimeString();
            }



            var Process1 =  System.Diagnostics.Process.Start("ffmpeg\\bin\\ffmpeg", prefix + " \"" + fileIn + "\" " + tmpCommand + " \"" + fileOut +"\"");
            if(nofastConvert)
                Process1.WaitForExit();
            return fileOut;
        }
    }
}
