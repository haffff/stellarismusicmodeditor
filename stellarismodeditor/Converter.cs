﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace stellarismodeditor
{
    public static class Converter
    {
        static private Ffmpeg converter = new Ffmpeg("-c libvorbis");



        public static string ToOGG(string file, string name )
        {
            string outFile = "converted\\";

            return converter.executeCommand(file, null, outFile + name+".ogg");
        }



        public static void ToMod(Dictionary<string, string> files, string name)
        {
            if (Directory.Exists(
                Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
                + "\\documents\\Paradox Interactive\\Stellaris\\mod\\" + name
                )) 
            {
                if(DialogResult.Yes == MessageBox.Show("Mod with that name already exists, Overwrite?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
                {
                    Directory.Delete(
                        Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
                + "\\documents\\Paradox Interactive\\Stellaris\\mod\\" + name
                );
                }
                else
                return;
            }
            if(String.IsNullOrEmpty(name) || String.IsNullOrWhiteSpace(name))
            {
                name = "unnamed";
            }

            foreach (KeyValuePair<string, string> a in files)
            {
                if (a.Value.Contains(".ogg"))
                {
                    File.Copy(a.Value, "converted\\" + a.Key + ".ogg");
                }
                else
                {
                    ToOGG(a.Value, a.Key);
                }

            }

            MessageBox.Show("When all black windows is gone click Ok");

            string[] mod = {
                "name=\"" + name + " ~ Created by IGRP(haffff) music mod maker\"",
                        "path=\"mod/" + name + "\"",
                        "tags={",
                        "   \"Sound\"",
                        "}",
                        "supported_version=\"2.1.3\""
                        };

            File.WriteAllLines(
                Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) 
                + "\\documents\\Paradox Interactive\\Stellaris\\mod\\" + name + ".mod", mod);

            Directory.CreateDirectory(
                Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
                + "\\documents\\Paradox Interactive\\Stellaris\\mod\\" + name);

            if (!Directory.Exists(Path.GetDirectoryName(Application.ExecutablePath) + "\\converted"))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(Application.ExecutablePath) + "\\converted");
            }

            Directory.Move(
                Path.GetDirectoryName(Application.ExecutablePath) 
                + "\\converted",Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
                + "\\documents\\Paradox Interactive\\Stellaris\\mod\\" + name + "\\music");


           


            foreach (string a in files.Keys)
                    {
                        string[] songs =
                        {
                            "song = {",
                            "    name = \""+a+"\"",
                            "}",
                            ""
                        };
                        File.AppendAllLines( Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) 
                            + "\\documents\\Paradox Interactive\\Stellaris\\mod\\" + name + "\\music\\"+name+".txt", songs );
                string[] songs1 =
               {
                            "music = {",
                            "   name = \"" + a + "\"",
                            "   file = \"" + a + ".ogg\"" ,
                            "   volume = 0.80" ,
                            "}",
                            ""
               };
                File.AppendAllLines(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\documents\\Paradox Interactive\\Stellaris\\mod\\" + name + "\\music\\" + name + ".asset",songs1);

            }
            
            MessageBox.Show("Mod was succefully created, thank you for patience");
        }

    }

}
