﻿namespace stellarismodeditor.Splitter
{
    partial class SplitterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitButton = new System.Windows.Forms.Button();
            this.Splittext = new System.Windows.Forms.RichTextBox();
            this.songsList = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.total = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // splitButton
            // 
            this.splitButton.Location = new System.Drawing.Point(12, 148);
            this.splitButton.Name = "splitButton";
            this.splitButton.Size = new System.Drawing.Size(60, 23);
            this.splitButton.TabIndex = 0;
            this.splitButton.Text = "Split";
            this.splitButton.UseVisualStyleBackColor = true;
            this.splitButton.Click += new System.EventHandler(this.splitButton_Click);
            // 
            // Splittext
            // 
            this.Splittext.Location = new System.Drawing.Point(139, 12);
            this.Splittext.Name = "Splittext";
            this.Splittext.Size = new System.Drawing.Size(405, 134);
            this.Splittext.TabIndex = 1;
            this.Splittext.Text = "xx:xx Name of song";
            // 
            // listBox1
            // 
            this.songsList.FormattingEnabled = true;
            this.songsList.Location = new System.Drawing.Point(13, 12);
            this.songsList.Name = "listBox1";
            this.songsList.Size = new System.Drawing.Size(120, 134);
            this.songsList.TabIndex = 2;
            this.songsList.DoubleClick += new System.EventHandler(this.songs_DoubleClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(78, 148);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.addAndClose_Click);
            // 
            // textBox1
            // 
            this.total.Location = new System.Drawing.Point(139, 152);
            this.total.Name = "textBox1";
            this.total.Size = new System.Drawing.Size(405, 20);
            this.total.TabIndex = 4;
            this.total.Text = "Total";
            // 
            // SplitterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 183);
            this.Controls.Add(this.total);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.songsList);
            this.Controls.Add(this.Splittext);
            this.Controls.Add(this.splitButton);
            this.Name = "SplitterForm";
            this.Text = "SplitterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button splitButton;
        private System.Windows.Forms.RichTextBox Splittext;
        private System.Windows.Forms.ListBox songsList;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox total;
    }
}