﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace stellarismodeditor.Splitter
{
    public partial class SplitterForm : Form
    {
        public SplitterForm(string filePath,MainForm parent)
        {
            this.filePath = filePath;
            InitializeComponent();
            this.parent = parent;
        }


        private MainForm parent;
        private string filePath;
        private string totalTime;
        private Ffmpeg splitter = new Ffmpeg("-ss [0] -to [1] -c copy");
        public List<string> files = new List<string>();


        private Dictionary<string,string> makeTimeTable()
        {
            Dictionary<string, string> tmp = new Dictionary<string, string>();
            try
            {
                foreach (string a in Splittext.Lines)
                {
                    if (a.Contains(" "))
                        tmp.Add(a.Substring(0, a.IndexOf(' ')), a.Remove(0, a.IndexOf(' ')));
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Something went wrong \n Exception: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
            tmp.Add(totalTime, "end");
            return tmp;
        }

        private void splitButton_Click(object sender, EventArgs e)
        {
            totalTime = total.Text;
            Dictionary<string, string> timeTable = makeTimeTable();
            if (timeTable == null || timeTable.Count == 0)
            {
                return;
            }
            List<string> keys = timeTable.Keys.ToList();
            List<string> vals = timeTable.Values.ToList();
            for (int i = 0; i < timeTable.Count; i++)
            {
                List<string> args = new List<string>();
                if (vals[i] == "end")
                {
                    break;
                }
                if (keys[i].Length == 5)
                {
                    keys[i].Insert(0, "00:");
                }
                if (keys[i+1].Length == 5)
                {
                    keys[i+1].Insert(0, "00:");
                }
                args.Add(keys[i]);
                args.Add(keys[i+1]);
                files.Add(splitter.executeCommand(filePath,args,"music\\"+vals[i].Remove(0,1)+".mp3"));
            }

            songsList.Items.AddRange(files.ToArray());
        }

        private void addAndClose_Click(object sender, EventArgs e)
        {

            foreach (string line in files)
            {
                parent.songs.Add(line.Remove(line.LastIndexOf('.')).Substring(line.LastIndexOf('\\') + 1), line);
            }
            parent.songs.Remove(filePath.Remove(filePath.LastIndexOf('.')).Substring(filePath.LastIndexOf('\\') + 1));
            parent.refreshLists();
            this.Close();
        }

        private void songs_DoubleClick(object sender, EventArgs e)
        {
            if (songsList.SelectedItem == null)
                return;
            files.Remove(songsList.SelectedItem.ToString());
            File.Delete(songsList.SelectedItem.ToString());
            songsList.Items.Remove(songsList.SelectedItem);
        }
    }
}
